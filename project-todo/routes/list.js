var express = require('express');
var app = express();
var router = express.Router();

var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({ extended: false });


/* S'il n'y a pas de todolist dans la session,
on en crée une vide sous forme d'array avant la suite */
app.use(function(req, res, next)
{
    if (typeof(req.session.todolist) == 'undefined')
    {
        req.session.todolist = [];
    }
    next();
})

router.get('/list', function(req, res, next)
{
    res.render('list',{todolist: req.session.todolist});
})

router.post('/list/ajouter/', urlencodedParser, function(req, res)
{
    if (req.body.newtodo != '')
    {
        req.session.todolist.push(req.body.newtodo);
    }
    res.redirect('/list');
})
router.get('/list/delete/:id', function(req,res)
{
    if (req.params.id != '')
    {
        req.session.todolist.splice(req.params.id,1);
    }
    res.redirect('/list');
})

module.exports = router;

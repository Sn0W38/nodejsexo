var express =       require('express');
var path =          require('path');
var favicon =       require('serve-favicon');
var logger =        require('morgan');
var cookieParser =  require('cookie-parser');
var bodyParser =    require('body-parser');
var sockIO =        require('socket.io')();
var ent =           require('ent');

//=============================Routes===========================================
var index =         require('./routes/index');
var users =         require('./routes/users');
var chat =          require('./routes/chat');
//==============================================================================

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/', chat);

// add Bootstrap
app.use('/js', express.static(__dirname+'/node_modules/bootstrap/dist/js'));
app.use('/css', express.static(__dirname+'/node_modules/bootstrap/dist/css'));
/*===============================socket========================================*/

app.sockIO = sockIO;

sockIO.on('connection', function (socket, pseudo)
{
    // quand un client se co au serveur
    console.log('un client se connect');

    socket.on('recupPseudo', function(pseudo, setPseudo, pseudoSav)
    {
        if (setPseudo != '')
        {
            pseudoSav = socket.pseudo;
            setPseudo = ent.encode(setPseudo);
            socket.pseudo = setPseudo;

            /*
            console.log('==================2==============');
            console.log('pseudo = '+pseudo);
            console.log('soket pseudo ='+socket.pseudo);

            console.log('pseudoSav = '+pseudoSav);
            console.log('setPseudo = '+setPseudo);
            */
        }
        else
        {
            pseudo = ent.encode(pseudo);
            socket.pseudo = pseudo;
        }
        //console.log(socket.setPseudo);
        //console.log(socket.pseudo);
        //console.log(socket.pseudoSav);

        /*Envoie le pseudo au client*/
        //socket.broadcast.emit('recupPseudo', pseudo);
        sockIO.emit('recupPseudo', pseudo,setPseudo,pseudoSav, {for : 'everyone'});
        /*if (socket.pseudo != '')
        {
            console.log(socket.pseudo+' est identifier');
        }
        else
        {
            console.log('pseudo vide');
        }*/
    });

    /*socket.on('modifPseudo', function(setPseudo)
    {
        setPseudo = ent.encode(setPseudo);
        socket.setPseudo = setPseudo;
        //console.log(socket.setPseudo);
        sockIO.emit('modifPseudo',pseudo, setPseudo, {for: 'everyone'});
    });*/

    socket.on('recupMsg', function(data)
    {
        message = ent.encode(data);
        socket.texto = message;
        sockIO.emit('recupMsg', {message: data, pseudo:socket.pseudo}, {for : 'everyone'});
        //console.log(socket.message);
        /*if (socket.texto != null )
        {
            console.log(socket.texto+' ecrit par ' + socket.pseudo);
        }
        else
        {
            console.log('message vide');
        }*/
    });
    socket.on('disconnect',function()
    {
        var pseudoDeco = socket.pseudo;
        //console.log(pseudoTest + ' disconnect');
        sockIO.emit('deconnexion',pseudoDeco ,{for : 'everyone'});
    });
});
/*===============================socket========================================*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
